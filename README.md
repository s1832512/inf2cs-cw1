# Description of the course work is likely to be updated! #

Please note that the description of the course work might be updated from time to time to clarify things or fix mistakes.

# Deadlines #
See the CW1 writeup.

# Scoreboard #
**TBD: need to host the scoreboard.**


We automatically run a series of hidden test programs using your compiler about twice a day.
You can keep track of your progress and see how many tests pass/fail using the scoreboard by following this link once the automarking will start:
[http://www.inf.ed.ac.uk/teaching/courses/ct/19-20/scoreboard/scoreboard.html](http://www.inf.ed.ac.uk/teaching/courses/ct/19-20/scoreboard/scoreboard.html)
The scorboard is provided as a best effort service, do not rely on it as it may come down unexpectedly: no guarantees is offered.

# Marking #

**TBC/TBD**

The marking will be done using an automated test suite on a dice machine using Java 8 (1.8 runtime).
The marking will be a function of the number of successful tests as a series of hidden tests.


# Setup #

## Register your student id and name

First, we will need you fill up [this google form](https://docs.google.com/forms/d/e/1FAIpQLSelKOz1yZZKaqFSd67Lq6i2KoHFGYd1BnyPWGGBue4ar4DylQ/viewform?usp=sf_link)
in order for us to register you for the automarking.
If you are not registered, we won't be able to mark you.
Also please make sure to keep `inf2cs-cw1` as your repository name, otherwise autmarking will fail.

## GitLab ##
We will rely on gitlab and it is mandatory to use it for this coursework.
GitLab is an online repository that can be used with the git control revision system.
The university runs a GitLab hosting service, and all students are provided with an account.
The username is your univeristy id number (sXXXXXXX) and your password is the EASE.
Please make sure to use your university id when login on the University gitlab, otherwise, we will be unable to automatically mark your coursework.

Important: do not share your code and repository with anyone and keep your source code secret.
If we identify that two students have identical portion of code, both will be considered to have cheated.


## Obtaining your own copy of the inf2cs-cw1 repository
We are going to be using the Git revision control system during the course. Git is installed on DICE machines. If you use your own machine then make sure to install Git.

You will need to have your own copy of the inf2cs-cw1 repository. In order to fork this repository, click the fork button:

![Forking the inf2cs-cw1 repository](/figures/gl_fork1.png "Forking this repository.")

![Forking the inf2cs-cw1 repository](/figures/gl_fork2.png "Forking this repository.")

Then, make the repository private

![Making repository private](/figures/gl_private1.png "Making repository private.")

![Making repository private](/figures/gl_private2.png "Making repository private.")

![Making repository private](/figures/gl_private3.png "Making repository private.")

Now, grant access to the teaching staff

![Granting the teaching staff read access](/figures/gl_permissions1.png "Granting the teaching staff read access.")

![Granting the teaching staff read access](/figures/gl_permissions2.png "Granting the teaching staff read access.")

You should grant the following users *Reporter* access:
  * Aaron Smith (username: asmith47)
  * Dmitrii Ustiugov (username: s1373190)  

Next, you will have to clone the forked repository to your local machine. In order to clone the repository you should launch a terminal and type:

```
$ git clone https://sXXXXXXX@git.ecdf.ed.ac.uk/sXXXXXXX/inf2cs-cw1.git
```

where sXXXXXXX is your student id


## Working with git and pushing your changes

Since we are using an automated marking mechnism (based on how many programs can run successfully through your compiler), it is important to understand how git works. If you want to benefit from the nightly automatic marking feedback, please ensure that you push all your changes daily onto your GitLab centralised repository.

We suggest you follow the excelent [tutorial](https://www.atlassian.com/git/tutorials/what-is-version-control) from atlassian on how to use git. In particular you will need to understand the following basic meachnisms:

* [add and commit](https://www.atlassian.com/git/tutorials/saving-changes)
* [push](https://www.atlassian.com/git/tutorials/syncing/git-push)

